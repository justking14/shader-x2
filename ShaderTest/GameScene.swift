//
//  GameScene.swift
//  ShaderTest
//
//  Created by Justin Buergi on 11/15/15.
//  Copyright (c) 2015 Justking Games. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    var myImage: UIImage!
    var hasFoundImage: Bool = false;
    var viewController: GameViewController!
    var hasClickedOnce:Bool = false;
    var moveDivisorDown: Bool = false
    var animating: Bool = false
    let sprite2 = SKSpriteNode(imageNamed:"Zelda_Map_-_Links_Awakening_shop_preview.png")
    var divisor: Float = 3;
    
    let chooseImage: SKLabelNode = SKLabelNode(fontNamed: "Chalkduster")
    let pixelateImage: SKLabelNode = SKLabelNode(fontNamed: "Chalkduster")
    let unPixelateImage: SKLabelNode = SKLabelNode(fontNamed: "Chalkduster")
    let saveImage: SKLabelNode = SKLabelNode(fontNamed: "Chalkduster")
    let animateImage: SKLabelNode = SKLabelNode(fontNamed: "Chalkduster")
    
    override func didMoveToView(view: SKView) {
        
        //let option = EAGLRenderingAPI
        //super.init(frame: frame, options: [ SKPreferredRenderingAPIKey : EAGLRenderingAPI.OpenGLES2.rawValue ])

        //super.init(frame: frame, options)
        /* Setup your scene here */
        let myLabel = SKLabelNode(fontNamed:"Chalkduster")
        myLabel.text = "Hello, World!";
        myLabel.fontSize = 45;
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame));
      
      //  self.addChild(myLabel)
        
        chooseImage.text = "Choose Picture"
        chooseImage.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 - 125)
        chooseImage.name = "Choose"
        self.addChild(chooseImage)
        
        pixelateImage.text = "Pixelate"
        pixelateImage.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 - 200)
        pixelateImage.name = "Pixelate"
        self.addChild(pixelateImage)
        
        unPixelateImage.text = "Un-Pixelate"
        unPixelateImage.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 - 275)
        unPixelateImage.name = "unPixelate"
        self.addChild(unPixelateImage)
        
        saveImage.text = "Save"
        saveImage.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 - 350)
        saveImage.name = "Save"
        self.addChild(saveImage)
        
        
        animateImage.text = "Animate"
        animateImage.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 - 375)
        animateImage.name = "Animate"
        self.addChild(animateImage)
        
        
        saveImage.zPosition = 500
        pixelateImage.zPosition = 500
        chooseImage.zPosition = 500
        
        
        sprite2.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 + 150)
        
        //let root = UIApplication.sharedApplication().keyWindow?.rootViewController;
        
        
        //let imagePic = UIImagePickerController()
       ////imagePic.delegate =
        
        
        
        //imagePic.delegate = self;
        
        self.addChild(sprite2)
    }
    func pixelDown(){
        if(divisor >= 16){
            moveDivisorDown = true
        }
        if(moveDivisorDown == true){
            divisor--
        }else{
            divisor++;
        }
        let shader: SKShader = SKShader(fileNamed: "RWTGradient2.fsh")
        let ratioX: Float = divisor/Float(sprite2.frame.size.width)
        let ratioY: Float = divisor/Float(sprite2.frame.size.height)
        shader.uniforms = [
            
            SKUniform(name: "u_gradient", texture: SKTexture(imageNamed: "circleshader")),
            SKUniform(name: "u_health", float: 0.75),
            SKUniform(name: "ratioX", float: ratioX),
            SKUniform(name: "ratioY", float: ratioY),
            
        ]
        sprite2.shader = shader;
        myImage = UIImage(CGImage: (sprite2.texture?.CGImage)!)
        
        let action = SKAction.sequence([SKAction.waitForDuration(0.05), SKAction.runBlock(pixelDown)])
        
        if(moveDivisorDown == true && divisor == 1){
            animating = false
            moveDivisorDown = false;
        }else{
            self.runAction(action)
        }
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            let touchedNode = nodeAtPoint(location)
            if(touchedNode.name == "Choose"){
                divisor = 1;
                self.viewController.photoFromLibrary()
                sprite2.shader = nil

            }else if(touchedNode.name == "Pixelate"){
                divisor++;
                
                let shader: SKShader = SKShader(fileNamed: "RWTGradient2.fsh")
                let ratioX: Float = divisor/Float(sprite2.frame.size.width)
                let ratioY: Float = divisor/Float(sprite2.frame.size.height)
                shader.uniforms = [
                    
                    SKUniform(name: "u_gradient", texture: SKTexture(imageNamed: "circleshader")),
                    SKUniform(name: "u_health", float: 0.75),
                    SKUniform(name: "ratioX", float: ratioX),
                    SKUniform(name: "ratioY", float: ratioY),
                    
                ]
                sprite2.shader = shader;
                myImage = UIImage(CGImage: (sprite2.texture?.CGImage)!)
                
                
            }else if(touchedNode.name == "Animate" && animating == false){
                animating = true
                self.pixelDown()
            }else if(touchedNode.name == "Save"){
                UIImageWriteToSavedPhotosAlbum(myImage!, self, "image:didFinishSavingWithError:contextInfo:", nil)
                
                
                var pngPath = NSHomeDirectory().stringByAppendingString("Documents/Test.png")
                var jpgPath = NSHomeDirectory().stringByAppendingString("Documents/Test.jpg")
                UIImagePNGRepresentation(myImage)?.writeToFile(pngPath, atomically: true)
                UIImageJPEGRepresentation(myImage, 1.0)?.writeToFile(jpgPath, atomically: true)
                
                /*var error: NSError;
                var fileMgr: NSFileManager = NSFileManager.defaultManager()
                var documentsDirectory = NSHomeDirectory().stringByAppendingString("Documents")
                
                
                let files = try fileMgr.contentsOfDirectoryAtPath("/Documents")
                print(files)*/
                
               // print(fileMgr.contentsOfDirectoryAtPath(documentsDirectory))
                //NSLog("Documents directory: %@", [fileMgr.contentsOfDirectoryAtPath:documentsDirectory, error:&error]);
               /* NSString  *pngPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Test.png"];
                NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Test.jpg"];
                
                // Write a UIImage to JPEG with minimum compression (best quality)
                // The value 'image' must be a UIImage object
                // The value '1.0' represents image compression quality as value from 0.0 to 1.0
                [UIImageJPEGRepresentation(image, 1.0) writeToFile:jpgPath atomically:YES];
                
                // Write image to PNG
                [UIImagePNGRepresentation(image) writeToFile:pngPath atomically:YES];*/
            }else if(touchedNode.name == "unPixelate"){
                divisor--;
                
                let shader: SKShader = SKShader(fileNamed: "RWTGradient2.fsh")
                let ratioX: Float = divisor/Float(sprite2.frame.size.width)
                let ratioY: Float = divisor/Float(sprite2.frame.size.height)
                shader.uniforms = [
                    
                    SKUniform(name: "u_gradient", texture: SKTexture(imageNamed: "circleshader")),
                    SKUniform(name: "u_health", float: 0.75),
                    SKUniform(name: "ratioX", float: ratioX),
                    SKUniform(name: "ratioY", float: ratioY),
                    
                ]
                sprite2.shader = shader;
                myImage = UIImage(CGImage: (sprite2.texture?.CGImage)!)
            }
            
            /*self.viewController.photoFromLibrary()
            
            let location = touch.locationInNode(self)
            divisor++;
            
            let shader: SKShader = SKShader(fileNamed: "RWTGradient2.fsh")
            let ratioX: Float = divisor/Float(sprite2.frame.size.width)
            let ratioY: Float = divisor/Float(sprite2.frame.size.height)
            shader.uniforms = [
                
                SKUniform(name: "u_gradient", texture: SKTexture(imageNamed: "circleshader")),
                SKUniform(name: "u_health", float: 0.75),
                SKUniform(name: "ratioX", float: ratioX),
                SKUniform(name: "ratioY", float: ratioY),
                
            ]
            if(hasClickedOnce == false){
                //sprite2.shader = shader;
                hasClickedOnce = true;
            }else{
            
            let sprite = SKSpriteNode(imageNamed:"Spaceship")
           sprite.zPosition = CGFloat(divisor)
            sprite.position = location
            sprite.shader = shader
            let action = SKAction.rotateByAngle(CGFloat(M_PI), duration:1)
            
            sprite.runAction(SKAction.repeatActionForever(action))
            
            self.addChild(sprite)*/
            //}
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        if(hasFoundImage == true){
            print(myImage)
            let node1a = SKSpriteNode(texture: SKTexture(image: myImage))
            node1a.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2)
            //self.addChild(node1a)
            sprite2.texture = SKTexture(image: myImage)
            hasFoundImage = false;
 
        }
    }
    
    func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafePointer<Void>) {
        if error == nil {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .Alert)
            ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            //presentViewController(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "Save error", message: error?.localizedDescription, preferredStyle: .Alert)
            ac.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
            //presentViewController(ac, animated: true, completion: nil)
        }
    }
    
    
}
