//
//  RWTGradient.fsh
//  RWTPixelShader
//
//  Created by Ricardo on 3/23/14.
//  Copyright (c) 2014 RayWenderlich. All rights reserved.
//

// Precision
highp float;
//uniform vec2 uResolution;
//uniform sampler2D tex;
void main() {
    float dx = ratioX;//divisor/width
    float dy = ratioY;//divisor/height
    
    float vecX = v_tex_coord.x/dx;//location of pixel
    float vecY = v_tex_coord.y/dy;
    
    vec2 pixelCoordinate = vec2(vecX, vecY);//pixel coordinate
    vec2 vector4 = vec2(floor(pixelCoordinate.x), floor(pixelCoordinate.y));
    
    float vecX2 = vector4.x * dx;//new location for x
    float vecY2 = vector4.y * dy;//new location for y
    
    vec2 vector5 = vec2(vecX2, vecY2);//new location of pixel
    
    
    gl_FragColor = texture2D(u_texture, vector5);//apply
    
}